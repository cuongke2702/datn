export const FETCH_USER = 'FETCH_USER';

export const DELETE_USER = 'DELETE_USER';

export const DELETE_CAT = 'DELETE_CAT';

export const UPDATE_USER = 'UPDATE_USER';

export const ADD_CAT = 'ADD_CAT';

export const GET_USER_BY_ID = 'GET_USER_BY_ID';

export const FETCH_CAT = 'FETCH_CAT1';

export const GET_CAT = 'GET_CAT';

export const UPDATE_CAT = 'UPDATE_CAT';

export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

export const FETCH_PRODUCT = 'FETCH_PRODUCT';

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const GET_PRODUCT_BY_ID = 'GET_PRODUCT_BY_ID';
export const ADD_PRODUCT = 'UPDATE_PRODUCT';