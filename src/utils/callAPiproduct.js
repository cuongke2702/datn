import axios from 'axios';
import * as Config from './Config';

export default function callAPiproduct(endpoint, method = 'GET', formdata) {
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        data: formdata,
    })

};