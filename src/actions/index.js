import * as Types from "../utils/ActionTypes";
import callAPiproduct from "../utils/callAPiproduct";
import callApi from "./../utils/apiCaller";

export const getUserAdmin = (user) => {
    return {
        type: Types.FETCH_USER,
        user,
    };

};

export const delUser = (id) => {
    return {
        type: Types.DELETE_USER,
        id,
    };
};

export const deleteUser = (id) => {
    return dispatch => {
        return callApi(`api/admin/user/del/${id}`, 'DELETE', null).then(res => {
            dispatch(delUser(id));
        })
    };
}

export const deleteCart = (id) => {
    return {
        type: Types.DELETE_CAT,
        id,
    };
};

export const deleteCatAPI = (id) => {
    return dispatch => {
        return callApi(`api/admin/cat/del/${id}`, 'DELETE', null).then(res => {
            dispatch(deleteCart(id));
        })
    };
}

// export const getUserAdmin = (token) =>{
//     return dispatch =>{
//         var decoded=jwt_decode(token)
//         console.log(token)
//         return callApiHeder(`api/admin/user/index`,'GET',null,token).then(res =>{
//             dispatch(getUserApiAdmin(data.data));
//         })
//     }
// }


export const getUserAdminCallApi = () => {
    return dispatch => {
        return callApi(`api/admin/user/index`, 'GET', null).then(res => {
            console.log(res.data.data);
            dispatch(getUserAdmin(res.data.data));
        })
    }
}

export const getCartAdmin = (cat) => {
    return {
        type: Types.FETCH_CAT,
        cat,
    };

};

export const getCartAdminCallApi = () => {
    return dispatch => {
        return callApi(`api/admin/cat/index`, 'GET', null).then(res => {
            dispatch(getCartAdmin(res.data.data));
        })
    }
}

export const getCatById = (cat) => {
    return {
        type: Types.GET_CAT,
        cat,
    }
}

export const getCatAPIById = (catId) => {
    return dispatch => {
        return callApi(`api/admin/cat/getcat/${catId}`, 'GET', null).then(res => {
            console.log('abc' ,res.data);
            dispatch(getCatById(res.data.data))
        })
    }
}

export const UpdateCatAPI = (cat) => async dispatch => {
    try {
        const { data } = await callApi(`api/admin/cat/edit/${cat.catId}`, 'POST', cat)
        console.log(data.data);
        return dispatch(updateCatById(data.data))
    } catch (error) {
        console.log(error.response)
    }
}

export const getFindAllProduct = (products) => {
    return {
        type: Types.FETCH_PRODUCT,
        products,
    }
}

export const getAllProductAPI = () => {
    return dispatch => {
        return callApi(`api/admin/product/getListProduct`, 'GET', null).then(res => {
            console.log(res.data.data);
            dispatch(getFindAllProduct(res.data.data))
        })
    }
}

export const addProductAction = (product) => {
    return {
        type: Types.ADD_PRODUCT,
        product,
    }
}

export const addProductApi = (formdata) => {
    return dispatch => {
        return callApi(`api/admin/product/uploadFile`,'POST',formdata).then(res => {
            console.log(res.data.data);
            dispatch(addProductAction(res.data.data))
        })
    }
}




// export const addStudentRequest= (student) =>{
//     return dispatch=>{
//         axios
//         .post(`http://localhost:8888/api/student/add`, {
//             studentId :student.studentId,
//             name:student.name,
//             tuoi:student.tuoi,
//             lop:{
//                 lopId:student.lopId,
//             }
//         })
//         .then(res => {
//             dispatch(addStudent(res.data))
//         }).catch(error => {
//             console.log(error.response);
//         })
//     }
// }

export const getUserById = (user) => {
    return {
        type: Types.GET_USER_BY_ID,
        user,
    }
}

export const getUserAPIRequest = (id) => {
    return dispatch => {
        return callApi(`api/admin/user/getuser/${id}`, 'GET', null).then(res => {
            dispatch(getUserById(res.data.data));
        })
    }
}

export const updateUserById = (user) => {
    return {
        type: Types.UPDATE_USER,
        user,
    }
}

export const updateUserAPICall = (user) => {
    return dispatch => {
        return callApi(`api/admin/user/update/${user.userId}`, 'PUT', user).then(res => {
            dispatch(updateUserById(res.data.data))
        })
    }
}

export const addCat = (cat) => {
    return {
        type: Types.ADD_CAT,
        cat,
    }
}

export const addCatAPI = (cat) => {
    return dispatch => {
        return callApi(`api/admin/cat/add`, 'POST', cat).then(res => {
            dispatch(addCat(res.data.data))
        })
    }
}


export const updateCatById = (cat) => {
    return {
        type: Types.UPDATE_CAT,
        cat,
    }
}

// export const UpdateCatAPI = (cat)=>{
//     return dispatch => {
//         return callApi(`api/admin/cat/edit/${cat.catId}`,'POST',cat).then(res=>{
//             dispatch(updateCatById(res.data.data))
//         })
//     }
// }



// export const updateStudent = (student) =>{
//     return {
//         type:"EDIT_STUDENT",
//         student,
//     }
// }

// export const updateStudentRequest= (student) =>{
//     console.log(student);
//     return dispatch=>{
//         axios
//         .post(`http://localhost:8888/api/student/edit/${student.studentId}`, {
//             studentId :student.studentId,
//             name:student.name,
//             tuoi:student.tuoi,
//             lop:{
//                 lopId:student.lopId,
//             }
//         })
//         .then(res => {
//             dispatch(updateStudent(res.data))
//         }).catch(error => {
//             console.log(error.response);
//         })
//     }
// }

// export const addStudentRequest2= (student) => async dispatch=>{
//     try {
//         const {data} =await axios.post(`http://localhost:8888/api/student/add`,{
//             studentId :student.studentId,
//             name:student.name,
//             tuoi:student.tuoi,
//             lop:{
//                 lopId:student.lopId,
//             }
//         })
//         return dispatch(addStudent(data))
//     } catch (error) {
//         console.log(error.response);
//     }
// }









