import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCartAdminCallApi, deleteCatAPI } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'

class CatList extends Component {
  componentDidMount() {
    this.props.fetchAPI();
  }
  delete = (id) => {
    this.props.deleteCat(id);
  }
  render() {
    const { catReducer } = this.props;
    console.log(catReducer);
    return (
      <div className="App">
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform">
          <table id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
            <thead>
              <tr>
                <th className="text-center">ID</th>
                <th className="text-center">UserName</th>
                <th className="text-center">Update</th>
                <th className="text-center">Add</th>
                <th className="text-center">Delete</th>
              </tr>
            </thead>
            <tbody>
              {
                catReducer.cat.map(data => (
                  <tr align="start">
                    <td className="text-center">{data.catId}</td>
                    <td className="text-center">{data.catName}</td>
                    <td className="text-center">
                      <a className="btn btn-primary" href={"/admin/cat/edit/" + data.catId}>Update</a>
                    </td>
                    <td className="text-center">
                      <a className="btn btn-primary" href="/admin/cat/add">Add</a>
                    </td>
                    <td className="text-center">
                      <button type="button" className="btn btn-danger" onClick={() => this.delete(data.catId)}>Delete</button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    catReducer: state
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAPI: () => {
      dispatch(getCartAdminCallApi())
    },
    deleteCat: (id) => {
      dispatch(deleteCatAPI(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CatList);