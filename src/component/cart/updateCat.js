import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { getCatAPIById, UpdateCatAPI } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'

class UpdateCat extends Component {

  constructor(props) {
    super(props);
    this.setState({
      catId: '',
    })
  }
  componentDidMount() {
    const { match } = this.props
    console.log(match);
    this.props.getCatById(match.params.catId)
    this.setState({
      catId: match.params.catId,
    })
  }

  // handleChange = (event) => {
  //     const target = event.target;
  //     const value = target.value;
  //     const name = target.name;
  //     this.setState({
  //         [name]: value,
  //     });
  //   };

  handelsubmit = (event) => {
    const cat = {
      catId: this.state.catId,
      catName: event.target.catName.value,
    }
    this.props.OnUpdate(cat);
    this.props.history.push('/admin/cat/index')
  }

  render() {
    const { editReducer } = this.props
    const { edit } = editReducer
    console.log(edit);
    return (
      <div>
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform">
          <Form onSubmit={this.handelsubmit}>
            <FormGroup>
              <Label for="catId">CatName</Label>
              <Input
                type="text"
                name="catName"
                id="catName"
                defaultValue={edit.catName}
              />
            </FormGroup>
            <FormGroup>
              <Button color="primary" type="submit">
                Save
              </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    getCatById: (catId) => {
      dispatch(getCatAPIById(catId));
    },
    OnUpdate: (cat) => {
      dispatch(UpdateCatAPI(cat))
    }
  }
}

const mapStateToProps = state => {
  return {
    editReducer: state
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCat);