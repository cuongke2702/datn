import { Component } from "react";
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  AppBar,
  Badge,
  Box,
  Hidden,
  IconButton,
  Toolbar
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';

class DashboardNavbar extends Component{

    render(){
        return (
            <AppBar
              elevation={0}
            >
              <Toolbar>
               
                <Box sx={{ flexGrow: 1 }} />
                <Hidden lgDown>
                  <IconButton color="inherit">
                    <Badge
                      color="primary"
                      variant="dot"
                    >
                      <NotificationsIcon />
                    </Badge>
                  </IconButton>
                  <IconButton color="inherit">
                    <InputIcon />
                  </IconButton>
                </Hidden>
                <Hidden lgUp>
                  <IconButton
                    color="inherit"
                  >
                    <MenuIcon />
                  </IconButton>
                </Hidden>
              </Toolbar>
            </AppBar>
          );
        };
    }
export default DashboardNavbar;