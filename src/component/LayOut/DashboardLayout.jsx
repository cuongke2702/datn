import { Component } from "react";
import UserList from "../user/userList";
import DashboardNavbar from "./DashboardNavbar";
import DashboardSidebar from "./DashboardSidebar";


class DashboardLayoutRoot extends Component{
    render(){
        return (
              <>
              <DashboardNavbar/>
              <DashboardSidebar/>
              <UserList></UserList>
              </>
          );
    }
}
export default DashboardLayoutRoot;