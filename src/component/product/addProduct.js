import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormText, FormGroup, Input, Label } from "reactstrap";
import { addProductApi, getCartAdminCallApi } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'

class ProductAdd extends Component {

    constructor(props) {
        super(props);
        this.setState({
            catId: '',
            catName: ''
        })
    }

    componentDidMount() {
        this.props.GetAllCat();
    }

    handelsubmit = (event) => {
        event.preventDefault();
        const productJson = {
            productId: event.target.productId.value,
            productName: event.target.productName.value,
            detail: event.target.detail.value,
            price: event.target.price.value,
            quantity: event.target.quantity.value,
            description: event.target.description.value,
            catId: "SP03",
            User_Id: 1
        }
            let formData = new FormData();
           formData.append('json', JSON.stringify(productJson))
            const image= event.target.FileTong.files
            const thumnails=event.target.myFile.files
            for(let i = 0; i< image.length; i++) {
                formData.append('FileTong', image[i])
            }
            for(let i = 0; i< thumnails.length; i++) {
                formData.append('myFile', thumnails[i])
            }
            this.props.addProduct(formData);
            this.props.history.push("/admin/product/index")
    }

    render() {
        const { catReducer } = this.props
        return (
            <div>
                <DashboardNavbar />
                <DashboardSidebar />
                <div className="divform" >
                    <Form onSubmit={this.handelsubmit}>
                        <FormGroup>
                            <Label for="productId">Product ID</Label>
                            <Input
                                type="text"
                                name="productId"
                                id="productId"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="productName">Product Name</Label>
                            <Input
                                type="text"
                                name="productName"
                                id="productName"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input
                                type="text"
                                name="description"
                                id="description"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="quantity">quantity</Label>
                            <Input
                                type="text"
                                name="quantity"
                                id="quantity"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="price">price</Label>
                            <Input
                                type="text"
                                name="price"
                                id="price"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="catId">CatId</Label>
                            <Input type="select" name="catId" id="catId">
                                {
                                    catReducer.cat.map(data => (
                                        <option defaultValue={data.catId}>{data.catName}</option>
                                    ))
                                }
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="myfile">Thumnails</Label>
                            <Input type="file" multiple name="myFile" id="myfile" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="FileTong">Images</Label>
                            <Input type="file" name="FileTong" id="FileTong" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="detail">Detail</Label>
                            <Input type="textarea" name="detail" id="detail" />
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" type="submit">
                                Save
                        </Button>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        GetAllCat: () => {
            dispatch(getCartAdminCallApi())
        },
        addProduct: (formData) => {
            dispatch(addProductApi(formData));
        }
    }
}

const mapStateToProps = state => {
    return {
        catReducer: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductAdd);