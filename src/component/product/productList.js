import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllProductAPI, deleteCatAPI } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'

class ProductList extends Component {

    componentDidMount() {
        this.props.fetchAPIproduct();
    }
    render() {

        const { productReducer } = this.props;
        return (
            <div className="App">
                <DashboardNavbar />
                <DashboardSidebar />
                <div className="divform" >
                    <table id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
                        <thead>
                            <tr>
                                <th className="text-center">ProductId</th>
                                <th className="text-center">Product Name</th>
                                <th className="text-center">Product Name</th>
                                <th className="text-center">Product Name</th>
                                <th className="text-center">Product Name</th>
                                <th className="text-center">Update</th>
                                <th className="text-center">Add</th>
                                <th className="text-center">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                productReducer.product.map(data => (
                                    <tr align="start">
                                        <td className="text-center">{data.productId}</td>
                                        <td className="text-center">{data.productName}</td>
                                        <td className="text-center">{data.price}</td>
                                        <td className="text-center">{data.quantity}</td>
                                        <td className="text-center">{data.images}</td>
                                        <td className="text-center">
                                            <a className="btn btn-primary" href={"/admin/cat/edit/" + data.catId}>Update</a>
                                        </td>
                                        <td className="text-center">
                                            <a className="btn btn-primary" href="/admin/product/add">Add</a>
                                        </td>
                                        <td className="text-center">
                                            <button type="button" className="btn btn-danger" onClick={() => this.delete(data.catId)}>Delete</button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        productReducer: state
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAPIproduct: () => {
            dispatch(getAllProductAPI())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);