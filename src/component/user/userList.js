import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteUser, getUserAdminCallApi } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'

class UserList extends Component {

  componentDidMount() {
    this.props.fetchAPI();
  }

  delete = (id) => {
    this.props.deleteUser(id);
  }


  render() {
    const { userReceduce } = this.props;
    console.log(this.props);
    return (
        <div className="App">
            <DashboardNavbar/>
              <DashboardSidebar/>
          <div className="divform" >
          <table id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
            <thead>
              <tr>
                <th className="text-center">ID</th>
                <th className="text-center">UserName</th>
                <th className="text-center">Email</th>
                <th className="text-center">Fullname</th>
                <th className="text-center">Phone</th>
                <th className="text-center">Update</th>
                <th className="text-center">Add</th>
                <th className="text-center">Delete</th>
              </tr>
            </thead>
            <tbody>
              {
                userReceduce.user.map(data => (
                  <tr align="start">
                    <td className="text-center">{data.userId}</td>
                    <td className="text-center">{data.userName}</td>
                    <td className="text-center">{data.email}</td>
                    <td className="text-center">{data.fullName}</td>
                    <td className="text-center">{data.phone}</td>
                    <td className="text-center">
                      <a className="btn btn-primary" href={"/admin/user/edit/" + data.userId}>Update</a>
                    </td>
                    <td className="text-center">
                      <button type="button" className="btn btn-danger" onClick={() => this.delete(data.userId)}>Delete</button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
          </div>
        </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    userReceduce: state
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAPI: () => {
      dispatch(getUserAdminCallApi())
    },
    deleteUser: (id) => {
      dispatch(deleteUser(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);