import * as Types from "../../utils/ActionTypes";

const initialState = {};

const editReducer =  (state = initialState,action)=>{
    switch(action.type){
        case Types.GET_USER_BY_ID: {
          state= action.user;
          return state
        }
        case Types.GET_CAT :{
            state=action.cat;
            return state
        }
        default:
          return state;
      }
}

export default editReducer;