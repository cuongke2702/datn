import { combineReducers  } from 'redux';
import userReceduce from './userReducer/user';
import catReducer from './cart/cat';
import editReducer from './userReducer/editUser';
import productReducer from './product/productReducer';
 const allReducers = combineReducers({

    user:userReceduce,
    cat:catReducer,
    edit:editReducer,
    product:productReducer,
});
export default allReducers;