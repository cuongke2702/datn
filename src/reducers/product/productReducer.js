import * as Types from "../../utils/ActionTypes";

const initialState = [];

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_PRODUCT: {
      state = [...action.products]
      console.log(state);
      return [...state];
    }
    // case Types.DELETE_CAT: {
    //   const newStateValue = []
    //   state.forEach(each => {
    //     if (each.catId.toString() !== action.id.toString()) {
    //       newStateValue.push(each)
    //     }
    //   })
    //   return [...newStateValue]
    // }
    case Types.ADD_PRODUCT: {
      state.push(action.product)
      return [...state]
    }
    // case Types.UPDATE_CAT: {
    //   state = [...state];
    //   console.log(state);
    //   console.log(action.cat.catName);
    //   const isExitCat = state.find(item => item.catId.toString() === action.cat.catId.toString())
    //   console.log(isExitCat);
    //   if (isExitCat) {
    //     state = state.map(each => {
    //       if (each.catId.toString() === action.cat.catId.toString()) {
    //         return {
    //           ...each,
    //           catName: action.cat.catName,
    //         }
    //       }
    //       return each
    //     })
    //   }
    //   if (!isExitCat) {
    //     state.push(action.cat)
    //   }
    //   return [...state]
    // }
    default:
      return [...state];
  }
}

export default productReducer;