import './App.css';
import React, { Component } from "react";
import UserList from "./component/user/userList";
import CatList from "./component/cart/catList";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import UpdateUser from './component/user/updateUser';
import addCat from './component/cart/addCat';
import updateCat from './component/cart/updateCat';
import productList from './component/product/productList';
import addProduct from './component/product/addProduct';
import DashboardLayoutRoot from './component/LayOut/DashboardLayout';

class App extends Component{

  render(){
    return(
      <Router>
        <Switch>
          <Route path='/admin/user/index' exact={true} component={UserList}/>
          <Route path='/admin/user/edit/:userId'  component={UpdateUser}/>
          <Route path='/admin/cat/index' component={CatList}/>
          <Route path='/admin/cat/add' component={addCat}/>
          <Route path='/admin/cat/edit/:catId' component={updateCat}/>
          <Route path='/admin/product/index' component={productList}/>
          <Route path='/admin/product/add' component={addProduct}/>
          <Route path='/admin/home' component={DashboardLayoutRoot}/>
        </Switch>
      </Router>
    )
  }
}

export default App;